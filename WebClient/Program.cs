using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.HttpSys;

namespace GoC.AppStatus.WebClient
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                 .UseHttpSys(options =>
                 {
                     options.Authentication.Schemes =
                         AuthenticationSchemes.NTLM |
                         AuthenticationSchemes.Negotiate;
                     options.Authentication.AllowAnonymous = false;
                 });
    }
}
