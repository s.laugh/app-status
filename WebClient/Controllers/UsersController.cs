﻿using GoC.WebTemplate.Components.Core.Services;
using Microsoft.AspNetCore.Mvc;

namespace GoC.AppStatus.WebClient.Controllers
{
    public class UsersController : BaseController
    {
        public UsersController(ModelAccessor modelAccessor) : base(modelAccessor) { }

        public IActionResult RegisterUser()
        {
            return View();
        }
    }
}