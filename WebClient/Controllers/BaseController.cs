﻿using GoC.AppStatus.WebClient.Models;
using GoC.WebTemplate.Components.Core.Services;
using GoC.WebTemplate.CoreMVC.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace GoC.AppStatus.WebClient.Controllers
{
    public class BaseController : WebTemplateBaseController
    {
        public BaseController(ModelAccessor modelAccessor) : base(modelAccessor) { }

        public BaseModel BaseModel { get; set; }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context)); //force failure if the context is null as the app can't run without it
            base.OnActionExecuting(context);
            string userName = context.HttpContext.User.Identity.Name;
            BaseModel = new BaseModel { UserName = userName };
        }

    }
}
