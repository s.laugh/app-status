﻿using GoC.WebTemplate.Components.Core.Services;
using Microsoft.AspNetCore.Mvc;

namespace GoC.AppStatus.WebClient.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(ModelAccessor modelAccessor) : base(modelAccessor) { }

        /* requested for controller localization, but not for view localization:
        public HomeController(IStringLocalizer<HomeController> localizer, ModelAccessor modelAccessor) : base(modelAccessor)
        {
            _localizer = localizer;
        }

        //private readonly IStringLocalizer<HomeController> _localizer;
        */

        public IActionResult Index()
        {
            // example of controller localization:
            // ViewData["MyTitle"] = _localizer["The localised title of my app!"];

            return View(BaseModel);
        }
    }
}