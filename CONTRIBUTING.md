# Contributing

## How to Contribute

Every employee in Government of Canada is welcome to suggest changes to this application, although those outside Employment and Social Development Canada may have some difficulties matching the [Development Environment Setup](#development-environment-setup).

When contributing, post comments and discuss changes you wish to make via Issues.

Feel free to propose changes by creating Merge Requests. If you don't have write access, editing a file will create a Fork of this project for you to save your proposed changes to. Submitting a change to a file will write it to a new Branch in your Fork, so you can send a Merge Request.

If this is your first time contributing on GitLab, don't worry! Let us know if you have any questions.

## Conduct Expectations

Please be aware of the [Code Of Conduct](CODE_OF_CONDUCT.md) expectations on behavior when contributing to the product.

## Security

**Mark security issues as confidential!**

## Making Changes

To start making changes you will need to Fork the project if you are not a member of it. You will also need to set up your [development enviornment](#development-environment-setup) and add a GitLab-Runner that meets the [Pipeline Configurations](#pipeline-configurations).

### Development Environment Setup

_Visual Studio 2019_ is the standard IDE for working on this project. _ASP.NET Core 2.1_ is **required** to run the project.

The [project wiki](https://gitlab.com/esdc-edsc/sds/app-status/-/wikis/home) further explains the 3rd party tools (dependancies) being used in the project.

### Pipeline Configurations

[GitLab-Runner](https://docs.gitlab.com/runner/install/) is need to be installed and registered against the project with a `shell` executor using `powershell` and have the following executables defined to the PATH:

* `nuget.exe`;
* `dotnet.exe`;

### Submitting for Review

Please follow the best practices defined in the ESDC recommendation for [Managing Merge Requests, Using Code Reivew](https://esdc-devcop.github.io/recommendations/merging-review.html) as the _author_.

### Reviewing a Merge Requests

Please follow the best practices defined in the ESDC recommendation for [Managing Merge Requests, Using Code Reivew](https://esdc-devcop.github.io/recommendations/merging-review.html) as the _reviewer_.
