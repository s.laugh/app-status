﻿using AutoFixture;
using AutoFixture.AutoNSubstitute;
using AutoFixture.Xunit2;
using GoC.AppStatus.WebClient.Controllers;

namespace GoC.AppStatus.WebClient.Test
{
    internal class AutoNSubstituteDataAttribute : AutoDataAttribute
    {
        public AutoNSubstituteDataAttribute()
            : base(() => new Fixture().Customize(new AppStatusCustomization()))
        { }
    }

    public class AppStatusCustomization : CompositeCustomization
    {
        public AppStatusCustomization()
            : base(new CoreCustomization(), new AutoNSubstituteCustomization())
        { }
    }

    public class CoreCustomization : ICustomization
    {
        public void Customize(IFixture fixture)
        {
            fixture.Customize<HomeController>(c => c.Without(p => p.ViewData));
            fixture.Customize<UsersController>(c => c.Without(p => p.ViewData));
        }
    }
}
