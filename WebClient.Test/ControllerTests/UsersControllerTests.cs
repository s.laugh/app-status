﻿using FluentAssertions;
using GoC.AppStatus.WebClient.Controllers;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace GoC.AppStatus.WebClient.Test.ControllerTests
{
    public class UsersControllerTests
    {
        [Theory, AutoNSubstituteData]
        public void RegisterUserReturnsAView(UsersController sut)
        {
            var result = sut.RegisterUser();
            result.Should().BeOfType<ViewResult>();
        }
    }
}
