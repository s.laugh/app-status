using FluentAssertions;
using GoC.AppStatus.WebClient.Controllers;
using GoC.AppStatus.WebClient.Models;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace GoC.AppStatus.WebClient.Test.ControllerTests
{
    public class HomeControllerTests
    {
        [Theory, AutoNSubstituteData]
        public void IndexReturnsAView(HomeController sut)
        {
            var result = sut.Index();
            result.Should().BeOfType<ViewResult>();
        }

        [Theory, AutoNSubstituteData]
        public void IndexReturnsUserNameInModel(HomeController sut)
        {
            // Arrange:
            string userName = "Test";
            BaseModel baseModel = new BaseModel { UserName = userName };
            sut.BaseModel = baseModel;

            // Act:
            IActionResult result = sut.Index();
            var resultModel = ((ViewResult)result).Model;

            // Assert:
            ((BaseModel)resultModel).UserName.Should().Be(userName);
        }

    }
}
